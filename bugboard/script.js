function myFunction() {
  var data = [];
  for (var page = 1; page < 10; page++) {
    var response = UrlFetchApp.fetch("https://gitlab.com/api/v4/groups/gitlab-org/issues?state=opened&per_page=100&labels=devops::release,bug&page=" + page);
    // Parse the JSON reply
    var json = response.getContentText();
    data = data.concat(JSON.parse(json));
    if (json.length < 100 ) {
      break;
    }
  }


  function getPriority(labels){
   if(labels.match(/P1/)){
     return "P1"
   } else if(labels.match(/P2/)){
     return "P2"
   } else if(labels.match(/P3/)){
     return "P3"
   } else if(labels.match(/P4/)){
     return "P4"
   } else {
     return " "
   }
  }

  function getSeverity(labels){
   if(labels.match(/S1/)){
     return "S1"
   } else if(labels.match(/S2/)){
     return "S2"
   } else if(labels.match(/S3/)){
     return "S3"
   } else if(labels.match(/S4/)){
     return "S4"
   } else {
     return " "
   }
  }

  function getFeature(labels){
    if(labels.match(/pages/i)){
      return "Pages"
    } else if(labels.match(/release orchestration/i)){
      return "Release Orchestration"
    } else if(labels.match(/merge trains/i)){
      return "Merge Trains"
    } else if(labels.match(/feature flags/i)){
      return "Feature Flags"
    } else {
      return ""
    }
  }

  function formatDate(date) {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
  }

  function getMilestone(milestone_object){
    if(milestone_object){
      return milestone_object["title"]
    } else {
      return ""
    }
  }

  function blocked(labels){
    if(labels.match(/awaiting feedback/i)){
      return true
    } else if(labels.match(/blocked/i)){
      return true
    } else {
      return ""
    }
  }

  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getActiveSheet();

  var output = [];

  output.push(["Title", "URL", "Created At", "Priority", "Severity", "Days Old", "SLO", "Milestone", "Feature", "Blocked", "Labels"]);

  data.forEach(function(elem,i) {
    output.push([elem["title"],elem["web_url"],formatDate(elem["created_at"]),getPriority(String(elem["labels"])),getSeverity(String(elem["labels"])),"=TODAY()-C" + (i+2), '=SWITCH(E' + (i+2) + ', "S1", 30, "S2", 60, "S3", 90, "S4", 120,"")', getMilestone(elem["milestone"]), getFeature(String(elem["labels"])), blocked(String(elem["labels"])), elem["labels"].join()]);
  });

  var len = output.length;

  // clear any previous content
  sheet.getRange(1,1,500,11).clearContent();

  // paste in the values
  sheet.getRange(1,1,len,11).setValues(output);
}
