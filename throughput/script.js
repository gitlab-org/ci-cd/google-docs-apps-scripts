function myFunction() {
  function fetchIdForUsername(username){
    var url = 'https://gitlab.com/api/v4/users?username=' + username;
    var response = UrlFetchApp.fetch(url);
    var json = response.getContentText();
    data = JSON.parse(json);
    return data[0]["id"];
  }

  function formatDate(date) {
    if(date == null){
      return ""
    } else {
      var d = new Date(date),
          month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }
  }

  function writeSpreadsheet(username){
    var data = [];
    var userId = fetchIdForUsername(username)
    var url = 'https://gitlab.com/api/v4/groups/gitlab-org/merge_requests?per_page=100&author_id=' + userId;

    for (var page = 1; page < 10; page++) {
      var response = UrlFetchApp.fetch(url + '&page=' + page);
      // Parse the JSON reply
      var json = response.getContentText();
      data = data.concat(JSON.parse(json));
      if (json.length < 100 ) {
        break;
      }
    }

    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = ss.getSheetByName(username)
    if (sheet == null) {
      sheet = ss.insertSheet(username);
    }

    var output = [];
    var columns = [
      "Title",
      "URL",
      "Created At",
      "Merged At",
      "State",
      "Created At Week"
    ]
    output.push(columns);

    data.forEach(function(elem,i) {
      output.push(
        [
          elem["title"],
          elem["web_url"],
          formatDate(elem["created_at"]),
          formatDate(elem["merged_at"]),
          elem["state"],
          '=C' + (i+2) + ' - WEEKDAY(C' + (i+2) + ',2) + 1'
        ]
      );
    });

    var len = output.length;

    // clear any previous content
    sheet.getRange(1,1,500,columns.length).clearContent();

    // paste in the values
    sheet.getRange(1,1,len,columns.length).setValues(output);
  }

  var engineers = [
    'dosuken123',
    'vshushlin',
    'jagood',
    'krasio',
    'ebaque',
    'sean_carroll',
    'afontaine',
    'nfriend',
    'sarahghp'
  ]

  engineers.forEach(function(engineer,i) {
    writeSpreadsheet(engineer);
  })
}
